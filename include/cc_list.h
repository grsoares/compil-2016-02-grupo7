#ifndef LIST_H
#define LIST_H
#include "cc_dict.h"

typedef struct _list {
    comp_dict_t* item;
    struct _list *next;
} comp_list_t;

comp_list_t* new_list();
void* get_top_item_list(comp_list_t* list);
comp_list_t* push_list(comp_list_t* list, comp_dict_t* item);
comp_list_t* remove_top_list(comp_list_t* list);
void free_list_item(comp_list_t* list);
void free_list(comp_list_t* list);

#endif
