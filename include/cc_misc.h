#ifndef __MISC_H
#define __MISC_H
#include <stdio.h>
#include "cc_tree.h"
/*
  Constantes a serem utilizadas para diferenciar os lexemas que estão
  registrados na tabela de símbolos.
*/
#define SIMBOLO_LITERAL_INT    1
#define SIMBOLO_LITERAL_FLOAT  2
#define SIMBOLO_LITERAL_CHAR   3
#define SIMBOLO_LITERAL_STRING 4
#define SIMBOLO_LITERAL_BOOL   5
#define SIMBOLO_IDENTIFICADOR  6

/* Escopos */
#define GLOBAL_SCP 1
#define LOCAL_SCP 2

/* Tipos primitivos */
#define IKS_INT 1
#define IKS_FLOAT 2
#define IKS_CHAR 3
#define IKS_STRING 4
#define IKS_BOOL 5

/* Tipos primitivos */
#define SIZE_INT 4
#define SIZE_FLOAT 8
#define SIZE_CHAR 1
#define SIZE_STRING 1
#define SIZE_BOOL 1

/* Usado para os 2 casos (primitivos e identificadores) */
#define IKS_UNKNOWN_TYPE 0

/* Tipos de identificadores */
#define IKS_FUNCTION 1 /* Identificador representa uma funcao */
#define IKS_VARIABLE 2 /* Identificador representa uma variavel */
#define IKS_VECTOR 3 /* Identificador representa um vetor */

#define IKS_SUCCESS 0 //caso não houver nenhum tipo de erro
/* Verificação de declarações */
#define IKS_ERROR_UNDECLARED 1 //identificador não declarado
#define IKS_ERROR_DECLARED 2 //identificador já declarado
/* Uso correto de identificadores */
#define IKS_ERROR_VARIABLE 3 //identificador deve ser utilizado como variável
#define IKS_ERROR_VECTOR 4 //identificador deve ser utilizado como vetor
#define IKS_ERROR_FUNCTION 5 //identificador deve ser utilizado como função
/* Tipos e tamanho de dados */
#define IKS_ERROR_WRONG_TYPE 6 //tipos incompatíveis
#define IKS_ERROR_STRING_TO_X 7 //coerção impossível do tipo string
#define IKS_ERROR_CHAR_TO_X 8 //coerção impossível do tipo char
/* Argumentos e parâmetros */
#define IKS_ERROR_MISSING_ARGS 9 //faltam argumentos
#define IKS_ERROR_EXCESS_ARGS 10 //sobram argumentos
#define IKS_ERROR_WRONG_TYPE_ARGS 11 //argumentos incompatíveis
/* Verificação de tipos em comandos */
#define IKS_ERROR_WRONG_PAR_INPUT 12 //parâmetro não é identificador
#define IKS_ERROR_WRONG_PAR_OUTPUT 13 //parâmetro não é literal string ou expressão
#define IKS_ERROR_WRONG_PAR_RETURN 14 //parâmetro não é expressão
//compatível com tipo do retorno
#define IKS_ERROR_NUMBER_INDEXES 15 // acesso a um vetor passando um numero de indices diferente de o de sua declaraçao

#define ILOC_NOP 0
#define ADD 1
#define SUB 2
#define MULT 3
#define DIV 4

#define ADDI 5
#define SUBI 6
#define RSUBI 7
#define MULTI 8
#define DIVI 9
#define RDIVI 10

#define LSHIFT 11
#define LSHIFTI 12
#define RSHIFT 13
#define RSHIFTI 14

#define AND 15
#define ANDI 16
#define OR 17
#define ORI 18
#define XOR 19
#define XORI 20

#define LOADI 21

#define LOAD 22
#define LOADAI 23
#define LOADAO 24

#define CLOAD 25
#define CLOADAI 26
#define CLOADAO 27

#define STORE 28
#define STOREAI 29
#define STOREAO 30

#define CSTORE 31
#define CSTOREAI 32
#define CSTOREAO 33

#define I2I 34
#define C2C 35
#define C2I 36
#define I2C 37

#define JUMPI 38
#define JUMP 39

#define CBR 40

#define CMP_LT 41
#define CMP_LE 42
#define CMP_EQ 43
#define CMP_GE 44
#define CMP_GT 45
#define CMP_NE 46

#define LABEL 50

typedef struct list_args_info {
    int info;
    struct list_args_info *next;
} list_args_info_t;

typedef struct symbol_table_value_struct {
    int lineNumber;
    int type;
    char* lexeme;
    union {
        float f;
        char c;
        int i;
        int b; //integer in the case of booleans since there's no boolean type in c
        char* s;
    } value;

    int primitive_type; //primitive type that the identifier should contain
    /* Used specially for identifiers */
    int identifier_type; //function, variable or vector.
    /* See constants above */

    // offset to codegen step
    long long int offset;
    
    int scope; // global or local
   
    list_args_info_t* args_info; // store size of dimensions for vectors, arg types for functions, null for variables and literals

} symbol_table_entry;

typedef struct iloc_code {
	int instruction;

	char* first_operator;
	char* second_operator;
	char* third_operator;

	struct iloc_code *prev;
} iloc_code_t;

typedef struct ast_node_value_struct {
    int semantic_type;
    symbol_table_entry* symbol_table_entry;
    union {
        float f;
        char c;
        int i;
        int b;
        char* s;
    } val;
    int primitive_type; //primitive type that the ast node should contain
  char* value_register;
    iloc_code_t* last_instruction;
} ast_node_value;


int getLineNumber (void);
void yyerror (char const *mensagem);
void main_init (int argc, char **argv);
void main_finalize (void);
list_args_info_t* new_list_args_info(int info, list_args_info_t* next);
void checkPositiveInt(struct symbol_table_value_struct *entry);
struct symbol_table_value_struct* checkIdentifierType(struct symbol_table_value_struct *entry, int targetType);
int getInferredType(int firstType, int secondType);
void checkValidCoercion(int sourceType, int targetType);
int checkDeclared(struct symbol_table_value_struct *entry);
void declare(int scope, struct symbol_table_value_struct *entry, int identifier_type, int primitive_type, list_args_info_t* args_info);
void checkParams(list_args_info_t* target, comp_tree_t* provided);
void checkIndexes(list_args_info_t* target, comp_tree_t* provided);
void end_scope();
void init_scope();
comp_tree_t* getFirstElementOfList(comp_tree_t* list);
ast_node_value* alloc_ast_value(int semantic_type);
long long int calculateOffset();
char* generateLabel();
char* generateRegister();
char* generateConst(long long int constValue);
void main_codegen(comp_tree_t* node);
void command_block_codegen(comp_tree_t* node);
void instruction_codegen(comp_tree_t* node);
void indexed_vector_codegen(comp_tree_t* node);
void variable_codegen(comp_tree_t* node);
void aritmetic_expression_codegen(comp_tree_t* node);
void aritmetic_operation_codegen(comp_tree_t* node, int semantic_type);
void boolean_params_condition_codegen(comp_tree_t* node, char* trueLabel, char* falseLabel);
void aritmetic_params_condition_codegen(comp_tree_t* node, char* trueLabel, char* falseLabel);
void condition_codegen(comp_tree_t* node, char* trueLabel, char* falseLabel);
void if_else_codegen(comp_tree_t* node);
void dowhile_codegen(comp_tree_t* node);
void whiledo_codegen(comp_tree_t* node);
iloc_code_t* getFirstIlocInstruction(comp_tree_t* node);
iloc_code_t* new_iloc(int instruction_type, char* first_operator, char* second_operator, char* third_operator, iloc_code_t* previous_instruction);
int getTypeSize(int type);
#endif
