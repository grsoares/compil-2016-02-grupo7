/*
  main.h

  Cabeçalho principal do analisador sintático
*/
#ifndef __MAIN_H
#define __MAIN_H
#include <stdio.h>
#include "cc_dict.h"
#include "cc_list.h"
#include "cc_tree.h"
#include "parser.h"

/*
  Constantes a serem utilizadas como valor de retorno no caso de
  sucesso (SINTATICA_SUCESSO) e erro (SINTATICA_ERRO) do analisador
  sintático.
*/
#define SINTATICA_SUCESSO 0
#define SINTATICA_ERRO    1

void cc_dict_etapa_1_print_entrada (char *token, int line);

#endif
