/*
  Grupo 7
  Bernardo de Freitas Zamperetti
  Gabriel Restori Soares
*/
%{
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cc_list.h"
#include "cc_misc.h"
#include "cc_ast.h"
#include "main.h"

extern comp_dict_t* symbolTable;
comp_list_t* scopeStack;
void *comp_tree_last;
int currentFunctionType = IKS_UNKNOWN_TYPE;

%}

/* Declaração dos tokens da linguagem */
%union {
    struct symbol_table_value_struct *valor_simbolo_lexico;
    struct comp_tree *ast;
    struct list_args_info *list_args_info;
    int primitive_type;
}

%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR
%token <valor_simbolo_lexico> TK_LIT_INT
%token <valor_simbolo_lexico> TK_LIT_FLOAT
%token <valor_simbolo_lexico> TK_LIT_FALSE
%token <valor_simbolo_lexico> TK_LIT_TRUE
%token <valor_simbolo_lexico> TK_LIT_CHAR
%token <valor_simbolo_lexico> TK_LIT_STRING
%token <valor_simbolo_lexico> TK_IDENTIFICADOR
%token TOKEN_ERRO

%nonassoc TK_PR_THEN
%nonassoc TK_PR_ELSE

%left '<' '>' TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE
%left '+' '-' TK_OC_OR TK_LIT_INT TK_LIT_FLOAT
%left '*' '/' TK_OC_AND


%type<primitive_type> unity_type primitive_type param

%type<ast> literal simple_expr expr_leaf function_call expr list_expr
%type<ast> list_expr_not_empty flow_control_instr shift_instr
%type<ast> command command_without_ending assign_left_allowed_leaf_expr
%type<ast> command_block list_commands decl_local_var_instr
%type<ast> assignment_instr input_instr output_instr return_instr
%type<ast> break_instr continue_instr function_call_instr comma_list_commands
%type<ast> decl_local_var_possibly_assigned list_commands_optional_ending
%type<ast> major_element list_major_element function simple_function static_function
%type<ast> decl_local_var_possibly_assigned_possibly_const_possibly_static
%type<ast> decl_local_var_possibly_assigned_possibly_const
%type<ast> bool_expr aritmetic_expr

%type<valor_simbolo_lexico> decl_local_var

%type<list_args_info> list_lit_int_positive_not_empty list_params list_params_not_empty


%start programa
%define parse.error verbose
%%


/* Regras (e ações) da gramática */

programa
  : init_scope list_major_element {
	if($2 != NULL) {
		comp_tree_last = tree_make_node(alloc_ast_value(AST_PROGRAMA));
		comp_tree_t* curr_children = getFirstElementOfList($2);
		while (curr_children != NULL) {
		    tree_insert_node(comp_tree_last, curr_children);
		    curr_children = curr_children->next;
		}
	} else {
	 	comp_tree_last = NULL;
	}
        end_scope();
  }
  | { comp_tree_last = NULL; }
  ;

init_scope
  :
  {
    init_scope();
  }



list_major_element
  : list_major_element major_element
  {
    if($1 == NULL) {
        $$ = $2;
    } else if ($2 == NULL) {
        $$ = $1;
    } else {
        $1->next = $2;
        $2->prev = $1;
        $$ = $2;
    }
  }
  | major_element { $$ = $1; } ;

major_element
  : decl_global_var_possibly_static { $$ = NULL; }
  | decl_new_type { $$ = NULL; }
  | function { $$ = $1; }
;

decl_global_var_possibly_static
  : TK_PR_STATIC decl_global_var
  | decl_global_var ;

decl_global_var
  : unity_type TK_IDENTIFICADOR ';'
    {
      declare(GLOBAL_SCP,$2, IKS_VARIABLE, $1, NULL);

    }
  | unity_type TK_IDENTIFICADOR '[' list_lit_int_positive_not_empty ']' ';'
    {
      declare(GLOBAL_SCP,$2, IKS_VECTOR, $1, $4);
    }
  ;

list_lit_int_positive_not_empty
  : list_lit_int_positive_not_empty ',' TK_LIT_INT
  {
    checkPositiveInt($3);
    $$ = new_list_args_info($3->value.i, $1);
  }
  | TK_LIT_INT
  {
    checkPositiveInt($1);
    $$ = new_list_args_info($1->value.i, NULL);
  }
  ;


unity_type
  : TK_IDENTIFICADOR { $$ = IKS_UNKNOWN_TYPE; }
  | primitive_type { $$ = $1; };

primitive_type
  : TK_PR_INT { $$ = IKS_INT; }
  | TK_PR_FLOAT { $$ = IKS_FLOAT; }
  | TK_PR_CHAR { $$ = IKS_CHAR; }
  | TK_PR_BOOL { $$ = IKS_BOOL; }
  | TK_PR_STRING { $$ = IKS_STRING; };

decl_new_type
  : TK_PR_CLASS TK_IDENTIFICADOR '[' list_fields ']' ';' ;

list_fields
  : list_fields_not_empty
  | ;

list_fields_not_empty
  : field
  | list_fields_not_empty ':' field ;

field
  : encaps primitive_type TK_IDENTIFICADOR ;

encaps
  : TK_PR_PRIVATE
  | TK_PR_PUBLIC
  | TK_PR_PROTECTED ;

function
  : static_function  { $$ = $1; }
  | simple_function  { $$ = $1; }
;

simple_function
  : unity_type TK_IDENTIFICADOR init_scope '(' list_params ')'
  {
    declare(LOCAL_SCP,$2, IKS_FUNCTION, $1, $5);
    currentFunctionType = $1;
  }
  '{' list_commands_optional_ending '}'
  {
    ast_node_value* value = alloc_ast_value(AST_FUNCAO);
    value->symbol_table_entry = $2;
    $$ = tree_make_node(value);
    comp_tree_t* curr_children = getFirstElementOfList($9);

    while (curr_children != NULL) {
	tree_insert_node($$, curr_children);
	curr_children = curr_children->next;
    }

    currentFunctionType = IKS_UNKNOWN_TYPE;
    end_scope();
  }
  ;

static_function
  : TK_PR_STATIC simple_function { $$ = $2; };

list_params
  : list_params_not_empty
  | { $$ = NULL; } ;

list_params_not_empty
  : param ',' list_params_not_empty
  {
    $$ = new_list_args_info($1, $3);
  }
  | param
  {
    $$ = new_list_args_info($1, NULL);
  }
  ;

param
  : unity_type TK_IDENTIFICADOR
  {
    declare(LOCAL_SCP, $2, IKS_VARIABLE, $1, NULL);
    $$ = $1;
  }
  | TK_PR_CONST unity_type TK_IDENTIFICADOR
  {
    declare(LOCAL_SCP, $3, IKS_VARIABLE, $2, NULL);
    $$ = $2;
  }
  ;

command_block
  : init_scope '{' list_commands_optional_ending '}'
  {
    $$ = tree_make_node(alloc_ast_value(AST_BLOCO));

    comp_tree_t* curr_children = getFirstElementOfList($3);

    while (curr_children != NULL) {
      tree_insert_node($$, curr_children);
      curr_children = curr_children->next;
    }

    end_scope();
  }
  ;

list_commands_optional_ending
  : list_commands command_without_ending {
    if ($1 == NULL){
      $$ = $2;
    } else if ($2 == NULL){
        $$ = $1;
    } else {
      $1->next = $2;
      $2->prev = $1;
      $$ = $2;
    }
  }
  ;

list_commands
  : list_commands command
  {
    if ($1 == NULL){
      $$ = $2;
    } else if ($2 == NULL){
        $$ = $1;
    } else {
      $1->next = $2;
      $2->prev = $1;
      $$ = $2;
    }
  }
  | list_commands case_instr { $$ = $1; }
  | { $$ = NULL; };

command
  : command_without_ending ';' { $$ = $1; }

command_without_ending
  : command_block { $$ = $1; }
  | decl_local_var_instr { $$ = $1; }
  | assignment_instr { $$ = $1; }
  | flow_control_instr { $$ = $1; }
  | input_instr { $$ = $1; }
  | output_instr { $$ = $1; }
  | return_instr { $$ = $1; }
  | break_instr { $$ = NULL; }
  | continue_instr { $$ = NULL; }
  | function_call_instr { $$ = $1; }
  | shift_instr  { $$ = $1; } ;
  | { $$ = NULL; } ;

comma_list_commands
  : comma_list_commands ',' command_without_ending {
        $1->next = $3;
        $3->prev = $1;
        $$ = $3;
  }
  | command_without_ending { $$ = $1; } ;

shift_instr
  : assign_left_allowed_leaf_expr TK_OC_SR TK_LIT_INT {
      checkPositiveInt($3);

      ast_node_value* literal_value = alloc_ast_value(AST_LITERAL);
      literal_value->symbol_table_entry = $3;
      comp_tree_t* literal_node = tree_make_node(literal_value);

      $$ = tree_make_binary_node(alloc_ast_value(AST_SHIFT_RIGHT), $1, literal_node);

    }
  | assign_left_allowed_leaf_expr TK_OC_SL TK_LIT_INT {
      checkPositiveInt($3);

      ast_node_value* literal_value = alloc_ast_value(AST_LITERAL);
      literal_value->symbol_table_entry = $3;
      comp_tree_t* literal_node = tree_make_node(literal_value);

      $$ = tree_make_binary_node(alloc_ast_value(AST_SHIFT_LEFT), $1, literal_node);
    } ;

return_instr
  : TK_PR_RETURN expr
    {
      int expression_type = ((ast_node_value*)($2->value))->primitive_type;
      checkValidCoercion(expression_type, currentFunctionType);
      $$ = tree_make_unary_node(alloc_ast_value(AST_RETURN), $2);
    }
  ;

break_instr
  : TK_PR_BREAK  { $$ = NULL; } ;

continue_instr
  : TK_PR_CONTINUE  { $$ = NULL; } ;

case_instr
  : TK_PR_CASE TK_LIT_INT  ;

decl_local_var_instr
  : decl_local_var_possibly_assigned_possibly_const_possibly_static { $$ = $1; } ;

decl_local_var_possibly_assigned_possibly_const_possibly_static
  : decl_local_var_possibly_assigned_possibly_const { $$ = $1; }
  | TK_PR_STATIC decl_local_var_possibly_assigned_possibly_const { $$ = $2; };

decl_local_var_possibly_assigned_possibly_const
  : decl_local_var_possibly_assigned { $$ = $1; }
  | TK_PR_CONST decl_local_var_possibly_assigned { $$ = $2; };

decl_local_var_possibly_assigned
  : decl_local_var { $$ = NULL; }
  | decl_local_var TK_OC_LE expr
  {
        ast_node_value* identifier_value = alloc_ast_value(AST_IDENTIFICADOR);
        identifier_value->symbol_table_entry = $1;
        comp_tree_t* identifier_node = tree_make_node(identifier_value);
        $$ = tree_make_binary_node(alloc_ast_value(AST_ATRIBUICAO), identifier_node, $3);

	checkValidCoercion(((ast_node_value*)($3->value))->primitive_type, $1->primitive_type);
  }
 ;


decl_local_var
  : primitive_type TK_IDENTIFICADOR
  {
    $$ = $2;
    declare(LOCAL_SCP, $$, IKS_VARIABLE, $1, NULL);
  }
  | primitive_type TK_IDENTIFICADOR '[' list_lit_int_positive_not_empty ']'
  {
    $$ = $2;
    declare(LOCAL_SCP, $$, IKS_VECTOR, $1, $4);
  }
  ;


assignment_instr
  : assign_left_allowed_leaf_expr '=' expr
  {
        $$ = tree_make_binary_node(alloc_ast_value(AST_ATRIBUICAO), $1, $3);
        checkValidCoercion(((ast_node_value*)($3->value))->primitive_type, ((ast_node_value*)($1->value))->primitive_type);
  }
  ;

input_instr
  : TK_PR_INPUT expr  {
    $$ = tree_make_unary_node(alloc_ast_value(AST_INPUT), $2);
  };

output_instr
  : TK_PR_OUTPUT list_expr {
    $$ = tree_make_node(alloc_ast_value(AST_OUTPUT));
    comp_tree_t* curr_children = getFirstElementOfList($2);

    while (curr_children != NULL) {
	tree_insert_node($$, curr_children);
	curr_children = curr_children->next;
    }
  } ;

list_expr
  : list_expr_not_empty { $$ = $1; }
  | { $$ = NULL; };

list_expr_not_empty
  : list_expr_not_empty ',' expr {
        $1->next = $3;
        $3->prev = $1;
        $$ = $3;
  }
  | expr { $$ = $1; } ;

function_call_instr
  : function_call { $$ = $1; } ;

flow_control_instr
  : TK_PR_IF '(' expr ')' TK_PR_THEN command {
        $$ = tree_make_binary_node(alloc_ast_value(AST_IF_ELSE), $3, $6);
  }
  | TK_PR_IF '(' expr ')' TK_PR_THEN command TK_PR_ELSE command {
        $$ = tree_make_ternary_node(alloc_ast_value(AST_IF_ELSE), $3, $6, $8);
  }
  | TK_PR_FOREACH '(' TK_IDENTIFICADOR ':' list_expr ')' command
  { $$ = NULL; }
  | TK_PR_FOR '(' comma_list_commands ':' expr ':' comma_list_commands ')' command
  {
    $$ = NULL;
  }
  | TK_PR_WHILE '(' expr ')' TK_PR_DO command {
        $$ = tree_make_binary_node(alloc_ast_value(AST_WHILE_DO), $3, $6);
  }
  | TK_PR_DO command TK_PR_WHILE '(' expr ')' {
        $$ = tree_make_binary_node(alloc_ast_value(AST_DO_WHILE), $2, $5);
  }
  | TK_PR_SWITCH '(' expr ')' command
  { $$ = NULL; }
  ;

expr
  : aritmetic_expr
  | bool_expr
  ;

aritmetic_expr
  : simple_expr { $$ = $1; }
  | aritmetic_expr '+' aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_ARIM_SOMA), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = getInferredType(
                    ((ast_node_value*)($1->value))->primitive_type,
                    ((ast_node_value*)($3->value))->primitive_type);
  }
  | aritmetic_expr '*' aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_ARIM_MULTIPLICACAO), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = getInferredType(
                    ((ast_node_value*)($1->value))->primitive_type,
                    ((ast_node_value*)($3->value))->primitive_type);
  }
  | aritmetic_expr '-' aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_ARIM_SUBTRACAO), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = getInferredType(
                    ((ast_node_value*)($1->value))->primitive_type,
                    ((ast_node_value*)($3->value))->primitive_type);
   }
  | aritmetic_expr '/' aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_ARIM_DIVISAO), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = getInferredType(
                    ((ast_node_value*)($1->value))->primitive_type,
                    ((ast_node_value*)($3->value))->primitive_type);
  }
  | aritmetic_expr TK_LIT_INT {
        ast_node_value* lit = alloc_ast_value(AST_LITERAL);
        lit->symbol_table_entry = $2;
        lit->val.i = $2->value.i;
        comp_tree_t* literal_node = tree_make_node(lit);
        $$ = tree_make_binary_node(alloc_ast_value(AST_ARIM_SOMA), $1, literal_node);
        ((ast_node_value*)($$->value))->primitive_type = getInferredType(
                      ((ast_node_value*)($1->value))->primitive_type,
                      $2->primitive_type);
  }
  | aritmetic_expr TK_LIT_FLOAT {
        ast_node_value* lit = alloc_ast_value(AST_LITERAL);
        lit->symbol_table_entry = $2;
        lit->val.f = $2->value.f;
        comp_tree_t* literal_node = tree_make_node(lit);
        $$ = tree_make_binary_node(alloc_ast_value(AST_ARIM_SOMA), $1, literal_node);
        ((ast_node_value*)($$->value))->primitive_type = getInferredType(
                      ((ast_node_value*)($1->value))->primitive_type,
                      $2->primitive_type);
  }
  | '-' simple_expr {
      $$ = tree_make_unary_node(alloc_ast_value(AST_ARIM_INVERSAO), $2);
      ((ast_node_value*)($$->value))->primitive_type = ((ast_node_value*)($2->value))->primitive_type;
  }

bool_expr
  :aritmetic_expr '<' aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_COMP_L), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
   }
  | aritmetic_expr '>' aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_COMP_G), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
  }
  | aritmetic_expr TK_OC_LE aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_COMP_LE), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
  }
  | aritmetic_expr TK_OC_GE aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_COMP_GE), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
  }
  | aritmetic_expr TK_OC_EQ aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_COMP_IGUAL), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
  }
  | aritmetic_expr TK_OC_NE aritmetic_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_COMP_DIF), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
  }
  | bool_expr TK_OC_AND bool_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_E), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type =IKS_BOOL;
  }
  | bool_expr TK_OC_OR bool_expr {
      $$ = tree_make_binary_node(alloc_ast_value(AST_LOGICO_OU), $1, $3);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
  }
  | '!' simple_expr {
      $$ = tree_make_unary_node(alloc_ast_value(AST_LOGICO_COMP_NEGACAO), $2);
      ((ast_node_value*)($$->value))->primitive_type = IKS_BOOL;
  }
  ;

simple_expr
  : '(' expr ')' { $$ = $2; }
  | expr_leaf    { $$ = $1; }
  ;

expr_leaf
  : assign_left_allowed_leaf_expr  { $$ = $1; }
  | literal       { $$ = $1; }
  | function_call { $$ = $1; }

assign_left_allowed_leaf_expr
  : TK_IDENTIFICADOR
  {
    int type = checkDeclared($1);
    struct symbol_table_value_struct
      *id_entry = checkIdentifierType($1, IKS_VARIABLE);
    ast_node_value* value = alloc_ast_value(AST_IDENTIFICADOR);
    value->symbol_table_entry = id_entry;
    value->primitive_type = type;
    $$ = tree_make_node(value);
  }
  | TK_IDENTIFICADOR '[' list_expr_not_empty ']'
  {
    int type = checkDeclared($1);
    struct symbol_table_value_struct
      *id_entry = checkIdentifierType($1, IKS_VECTOR);
    checkIndexes(id_entry->args_info, $3);

    ast_node_value* identifier_value = alloc_ast_value(AST_IDENTIFICADOR);
    identifier_value->symbol_table_entry = id_entry;
    identifier_value->primitive_type = type;
    comp_tree_t* identifier_node = tree_make_node(identifier_value);
    $$ = tree_make_unary_node(alloc_ast_value(AST_VETOR_INDEXADO), identifier_node);
    ((ast_node_value*)($$->value))->primitive_type = type;


    comp_tree_t* curr_children = getFirstElementOfList($3);
    while (curr_children != NULL) {

         tree_insert_node($$, curr_children);

	 checkValidCoercion(((ast_node_value*)(curr_children->value))->primitive_type, IKS_INT);

         curr_children = curr_children->next;
    }


  }

function_call
  : TK_IDENTIFICADOR '(' list_expr ')'
  {
      ast_node_value* identifier_value = alloc_ast_value(AST_IDENTIFICADOR);

      identifier_value->symbol_table_entry = $1;
      comp_tree_t* identifier_node = tree_make_node(identifier_value);
      $$ = tree_make_unary_node(alloc_ast_value(AST_CHAMADA_DE_FUNCAO), identifier_node);

      ((ast_node_value*)($$->value))->primitive_type = checkDeclared($1);
      struct symbol_table_value_struct *function_entry = checkIdentifierType($1, IKS_FUNCTION);

      checkParams(function_entry->args_info, $3);

      comp_tree_t* curr_children = getFirstElementOfList($3);
      while (curr_children != NULL) {
	tree_insert_node($$, curr_children);
        curr_children = curr_children->next;
      }
  }
;

literal
  : TK_LIT_INT
  { ast_node_value* value = alloc_ast_value(AST_LITERAL);
    value->symbol_table_entry = $1;
    value->primitive_type = IKS_INT;
    value->val.i = $1->value.i;
    $$ = tree_make_node(value);
  }
  | TK_LIT_FLOAT
  { ast_node_value* value = alloc_ast_value(AST_LITERAL);
    value->symbol_table_entry = $1;
    value->primitive_type = IKS_FLOAT;
    value->val.f = $1->value.f;
    $$ = tree_make_node(value);
  }
  | TK_LIT_FALSE
  { ast_node_value* value = alloc_ast_value(AST_LITERAL);
    value->symbol_table_entry = $1;
    value->primitive_type = IKS_BOOL;
    value->val.b = $1->value.b;
    $$ = tree_make_node(value);
  }
  | TK_LIT_TRUE
  { ast_node_value* value = alloc_ast_value(AST_LITERAL);
    value->symbol_table_entry = $1;
    value->primitive_type = IKS_BOOL;
    value->val.b = $1->value.b;
    $$ = tree_make_node(value);
  }
  | TK_LIT_CHAR
  { ast_node_value* value = alloc_ast_value(AST_LITERAL);
    value->symbol_table_entry = $1;
    value->primitive_type = IKS_CHAR;
    value->val.c = $1->value.c;
    $$ = tree_make_node(value);
  }
  | TK_LIT_STRING
  {
    ast_node_value* value = alloc_ast_value(AST_LITERAL);
    value->symbol_table_entry = $1;
    value->primitive_type = IKS_STRING;
    value->val.s = $1->value.s;
    $$ = tree_make_node(value);
  }
;
