/*
  Grupo 7
  Bernardo de Freitas Zamperetti
  Gabriel Restori Soares
*/
%{
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "cc_misc.h"
#include "cc_dict.h"
#include "main.h"
#include <stdlib.h>
#include <string.h>

int lineNumber;
comp_dict_t* symbolTable;

//returns a pointer to the entry in the symbol table
symbol_table_entry* insertSymbolOnTable(char* lexeme, int type) {
    int size = strlen(lexeme);
    char* key = (char*)malloc(sizeof(char)*(size + 2));

    //adds type to the key in the table
    strcpy(key, lexeme);
    key[size] = type;
    key[size+1] = '\0';
    symbol_table_entry *entry = (symbol_table_entry*)malloc(sizeof(symbol_table_entry));

    entry->lineNumber = lineNumber;
    entry->type = type;
    entry->offset = -1;
    char *new_lexeme = strdup(lexeme);
    char *string_value;

    switch(type) {
        case SIMBOLO_LITERAL_INT: //in the case it's an integer literal
            entry->value.i = atoi(lexeme);
            entry->lexeme = new_lexeme;
            entry->primitive_type = IKS_INT;
            break;
        case SIMBOLO_LITERAL_FLOAT: //in the case it's a float literal
            entry->value.f = (float) atof(lexeme);
            entry->lexeme = new_lexeme;
            entry->primitive_type = IKS_FLOAT;
            break;
        case SIMBOLO_LITERAL_CHAR: //in the case it's a char literal
            entry->value.c = lexeme[1];
            entry->lexeme = (char*)malloc(sizeof(char)*2);
            entry->lexeme[0] = lexeme[1];
            entry->lexeme[1] = '\0';
            entry->primitive_type = IKS_CHAR;
            free(new_lexeme);
            break;
        case SIMBOLO_LITERAL_STRING: //in the case it's a string literal
            //allocates the right size to contain the string without quotes and with the '\0' character
            string_value = (char*)malloc(sizeof(char) * (size - 1));
            strncpy(string_value, lexeme + 1, size - 2); //copies without quotes
            string_value[size - 2] = '\0';
            entry->value.s = string_value;
            entry->lexeme = string_value;
            entry->primitive_type = IKS_STRING;
            free(new_lexeme);
            break;
        case SIMBOLO_LITERAL_BOOL: //in the case it's a bool literal
            entry->value.b = strcmp(lexeme, "true") == 0;
            entry->lexeme = new_lexeme;
            entry->primitive_type = IKS_BOOL;
            //value gets 1 if it's the true literal and 0 if it's the false one
            break;
		case SIMBOLO_IDENTIFICADOR: //in the case it's an identifier
            entry->lexeme = new_lexeme;
            entry->value.s = new_lexeme;
            entry->primitive_type = IKS_UNKNOWN_TYPE;
            entry->identifier_type = IKS_UNKNOWN_TYPE;
        default:
            break;
    }

    symbol_table_entry* inserted_entry = dict_put(symbolTable, key, entry);

    free(key);
    return inserted_entry;
}
%}

white [ \t]+
digit [0-9]
letter [a-zA-Z]

%x COMMENT
%%
{white} { }
\n {lineNumber++;}

int return TK_PR_INT;
float return TK_PR_FLOAT;
bool return TK_PR_BOOL;
char return TK_PR_CHAR;
string return TK_PR_STRING;
if return TK_PR_IF;
then return TK_PR_THEN;
else return TK_PR_ELSE;
while return TK_PR_WHILE;
do return TK_PR_DO;
input return TK_PR_INPUT;
output return TK_PR_OUTPUT;
return return TK_PR_RETURN;
const return TK_PR_CONST;
static return TK_PR_STATIC;
foreach return TK_PR_FOREACH;
for return TK_PR_FOR;
switch return TK_PR_SWITCH;
case return TK_PR_CASE;
break return TK_PR_BREAK;
continue return TK_PR_CONTINUE;
class return TK_PR_CLASS;
private return TK_PR_PRIVATE;
public return TK_PR_PUBLIC;
protected return TK_PR_PROTECTED;

\,|\;|\:|\(|\)|\[|\]|\{|\}|\+|\-|\*|\/|\<|\>|\=|\!|\&|\$|\%|\#|\^ return yytext[0];

\<\= return TK_OC_LE;
\>\= return TK_OC_GE;
\=\= return TK_OC_EQ;
\!\= return TK_OC_NE;
\&\& return TK_OC_AND;
\|\| return TK_OC_OR;
\>\> return TK_OC_SR;
\<\< return TK_OC_SL;

{digit}+ {
    yylval.valor_simbolo_lexico = insertSymbolOnTable(yytext, SIMBOLO_LITERAL_INT);
    return TK_LIT_INT;
}

{digit}+\.{digit}+ {
    yylval.valor_simbolo_lexico = insertSymbolOnTable(yytext, SIMBOLO_LITERAL_FLOAT);
    return TK_LIT_FLOAT;
}

'.' {
    yylval.valor_simbolo_lexico = insertSymbolOnTable(yytext, SIMBOLO_LITERAL_CHAR);
    return TK_LIT_CHAR;
}

\"[^\"]*\" {
    yylval.valor_simbolo_lexico = insertSymbolOnTable(yytext, SIMBOLO_LITERAL_STRING);
    return TK_LIT_STRING;
}

false {
    yylval.valor_simbolo_lexico = insertSymbolOnTable(yytext, SIMBOLO_LITERAL_BOOL);
    return TK_LIT_FALSE;
}

true {
    yylval.valor_simbolo_lexico = insertSymbolOnTable(yytext, SIMBOLO_LITERAL_BOOL);
    return TK_LIT_TRUE;
}

({letter}|_)({letter}|{digit}|_)* {
    yylval.valor_simbolo_lexico = insertSymbolOnTable(yytext, SIMBOLO_IDENTIFICADOR);
    return TK_IDENTIFICADOR;
}

"//".*\n {lineNumber++;}

"/*" BEGIN(COMMENT);
<COMMENT>[^*\n]*
<COMMENT>"*"+[^*/\n]*
<COMMENT>\n {lineNumber++;}
<COMMENT>"*"+"/" BEGIN(INITIAL);

. return TOKEN_ERRO;
%%
