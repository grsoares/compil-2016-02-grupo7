#include "cc_list.h"
#include "cc_dict.h"
#include <stdlib.h>

comp_list_t* new_list()
{
    return NULL;
}

comp_list_t* push_list(comp_list_t* list, comp_dict_t* item)
{
    comp_list_t* top = (comp_list_t*) malloc(sizeof(comp_list_t));
    top->item = item;
    top->next = list;
    return top;
}

comp_list_t* remove_top_list(comp_list_t* list)
{
    if (list == NULL) return NULL;
    comp_list_t* top = list;
    list = list->next;
    free(top);
    return list;
}

void* get_top_item_list(comp_list_t* list){
    if (list == NULL)
      return NULL;
    return list->item;
}

void free_list_item(comp_list_t* list)
{
    if (list == NULL) return;
    if (list->item != NULL)
        dict_free(list->item);
    free(list);
}

void free_list(comp_list_t* list)
{
    if (list == NULL) return;
    while (list != NULL)
    {
        comp_list_t* n = list->next;
        free_list_item(list);
        list = n;
    }
}
