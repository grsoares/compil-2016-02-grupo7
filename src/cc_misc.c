#include "cc_misc.h"
#include "cc_tree.h"
#include "cc_gv.h"
#include "cc_ast.h"
#include "cc_dict.h"
#include "cc_list.h"
#include "main.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

extern int lineNumber;
extern comp_dict_t* symbolTable;
extern comp_list_t* scopeStack;
extern void *comp_tree_last;
comp_list_t* allScopes;

char* rarp;
char* rbss;
/*
 todas as funcoes codegen apenas vao preencher o first instruction
e last instruction pointer do nodo passado como parametro e seus filhos,
e criar as estruturas de iloc code
*/
void main_codegen(comp_tree_t* node){
  rarp = (char*)malloc(sizeof(char)*12);
  sprintf(rarp, "rarp");
  rbss = (char*)malloc(sizeof(char)*12);
  sprintf(rbss, "rbss");

  command_block_codegen(node);
}

void command_block_codegen(comp_tree_t* node){
  comp_tree_t* curr_children = node->first;
  iloc_code_t* curr_last_instruction = NULL;

  while (curr_children != NULL){

      instruction_codegen(curr_children);

      // se nao tem nenhuma instruçao so continua...
      if (curr_children->value->last_instruction == NULL){
	curr_children = curr_children->next;
	continue;
      }

      iloc_code_t* first_iloc_instr_of_curr_children = getFirstIlocInstruction(curr_children);

      first_iloc_instr_of_curr_children->prev = curr_last_instruction;


      curr_last_instruction = curr_children->value->last_instruction;
      curr_children = curr_children->next;
  }

  node->value->last_instruction = curr_last_instruction;
}

void instruction_codegen(comp_tree_t* node){
    switch(node->value->semantic_type) {
    case AST_ATRIBUICAO:
      if (node->first->value->semantic_type == AST_VETOR_INDEXADO)
	indexed_vector_codegen(node->first);
      else
	variable_codegen(node->first);

      iloc_code_t* curr_last_instruction = node->first->value->last_instruction;
      aritmetic_expression_codegen(node->last);
      iloc_code_t* first_iloc = getFirstIlocInstruction(node->last);
      first_iloc->prev = curr_last_instruction;
      curr_last_instruction = node->last->value->last_instruction;
      node->value->last_instruction =
      new_iloc(STORE, node->last->value->value_register, node->first->value->value_register, NULL, curr_last_instruction);
      break;
    case AST_IF_ELSE:
      if_else_codegen(node);
      break;
    case AST_DO_WHILE:
      dowhile_codegen(node);
      break;
    case AST_WHILE_DO:
      whiledo_codegen(node);
      break;
    default:
      break;
    }
}

void if_else_codegen(comp_tree_t* node){
  char* trueLabel = generateLabel();
  char* falseLabel = generateLabel();
  char* endLabel = generateLabel();
  condition_codegen(node->first, trueLabel, falseLabel);
  node->value->last_instruction = node->first->value->last_instruction;
  node->value->last_instruction =
    new_iloc(LABEL, trueLabel, NULL,  NULL, node->value->last_instruction);

  command_block_codegen(node->first->next);
  iloc_code_t* first_iloc = getFirstIlocInstruction(node->first->next);
  first_iloc->prev = node->value->last_instruction;
  node->value->last_instruction = node->first->next->value->last_instruction;

  node->value->last_instruction =
    new_iloc(JUMPI, endLabel, NULL,  NULL, node->value->last_instruction);

  node->value->last_instruction =
    new_iloc(LABEL, falseLabel, NULL,  NULL, node->value->last_instruction);

  if (node->first->next->next != NULL){ // has else
    command_block_codegen(node->first->next->next);
    first_iloc = getFirstIlocInstruction(node->first->next->next);
    first_iloc->prev = node->value->last_instruction;
    node->value->last_instruction = node->first->next->next->value->last_instruction;
  } else {
    node->value->last_instruction =
      new_iloc(JUMPI, endLabel, NULL,  NULL, node->value->last_instruction);
  }

  node->value->last_instruction =
    new_iloc(LABEL, endLabel, NULL,  NULL, node->value->last_instruction);
}

void dowhile_codegen(comp_tree_t* node){
  char* trueLabel = generateLabel();
  char* falseLabel = generateLabel();

  node->value->last_instruction =
    new_iloc(LABEL, trueLabel, NULL,  NULL, NULL);

  command_block_codegen(node->first);
  iloc_code_t* first_iloc = getFirstIlocInstruction(node->first);
  first_iloc->prev = node->value->last_instruction;
  node->value->last_instruction = node->first->value->last_instruction;

  condition_codegen(node->last, trueLabel, falseLabel);
  first_iloc = getFirstIlocInstruction(node->last);
  first_iloc->prev = node->value->last_instruction;
  node->value->last_instruction = node->last->value->last_instruction;

  node->value->last_instruction =
    new_iloc(LABEL, falseLabel, NULL,  NULL, node->value->last_instruction);
}

void whiledo_codegen(comp_tree_t* node){
  char* trueLabel = generateLabel();
  char* falseLabel = generateLabel();
  char* initLabel = generateLabel();

  node->value->last_instruction =
    new_iloc(LABEL, initLabel, NULL,  NULL, NULL);
  
  condition_codegen(node->first, trueLabel, falseLabel);
  iloc_code_t* first_iloc = getFirstIlocInstruction(node->first);
  first_iloc->prev = node->value->last_instruction;
  node->value->last_instruction = node->first->value->last_instruction;

  node->value->last_instruction =
    new_iloc(LABEL, trueLabel, NULL,  NULL, node->value->last_instruction);

  command_block_codegen(node->last);
  first_iloc = getFirstIlocInstruction(node->last);
  first_iloc->prev = node->value->last_instruction;
  node->value->last_instruction = node->last->value->last_instruction;

  node->value->last_instruction =
    new_iloc(JUMPI, initLabel, NULL,  NULL, node->value->last_instruction);

  node->value->last_instruction =
    new_iloc(LABEL, falseLabel, NULL,  NULL, node->value->last_instruction);
}


void aritmetic_params_condition_codegen(comp_tree_t* node, char* trueLabel, char* falseLabel){
    aritmetic_expression_codegen(node->first);
    aritmetic_expression_codegen(node->last);
    iloc_code_t* first_from_last = getFirstIlocInstruction(node->last);
    first_from_last->prev = node->first->value->last_instruction;

    node->value->value_register = generateRegister();

    int comp_iloc_instr = 0;
    switch(node->value->semantic_type) {
    case AST_LOGICO_COMP_DIF: comp_iloc_instr = CMP_NE;  break;
    case AST_LOGICO_COMP_IGUAL: comp_iloc_instr = CMP_EQ;  break;
    case AST_LOGICO_COMP_LE: comp_iloc_instr = CMP_LE;  break;
    case AST_LOGICO_COMP_GE: comp_iloc_instr = CMP_GE;  break;
    case AST_LOGICO_COMP_L: comp_iloc_instr = CMP_LT;  break;
    case AST_LOGICO_COMP_G: comp_iloc_instr = CMP_GT;  break;
        default:
            break;
    }
    node->value->last_instruction =
        new_iloc(comp_iloc_instr, node->first->value->value_register, node->last->value->value_register, node->value->value_register, node->last->value->last_instruction);

    node->value->last_instruction =
      new_iloc(CBR, node->value->value_register, trueLabel, falseLabel, node->value->last_instruction);
}

void boolean_params_condition_codegen(comp_tree_t* node, char* trueLabel, char* falseLabel){

  char* continueLabel = generateLabel();

    switch(node->value->semantic_type) {
    case AST_LOGICO_E:
      condition_codegen(node->first, continueLabel, falseLabel);
      break;
    case AST_LOGICO_OU:
      condition_codegen(node->first, trueLabel, continueLabel);
     break;

        default:
            break;
    }

    node->value->last_instruction = node->first->value->last_instruction;
    node->value->last_instruction =
	new_iloc(LABEL, continueLabel, NULL, NULL, node->value->last_instruction);
    condition_codegen(node->last, trueLabel, falseLabel);
    iloc_code_t* first_iloc = getFirstIlocInstruction(node->last);
    first_iloc->prev = node->value->last_instruction;
    node->value->last_instruction = node->last->value->last_instruction;
}

void condition_codegen(comp_tree_t* node, char* trueLabel, char* falseLabel){

  if (node->first->value->primitive_type != node->last->value->primitive_type
      ){
    printf("ERRO NA PRECEDENCIA");
    exit(0);
  }

  if (node->first->value->primitive_type != IKS_BOOL){
    aritmetic_params_condition_codegen(node,trueLabel,falseLabel);
  } else  {
    boolean_params_condition_codegen(node,trueLabel,falseLabel);
  }
}


void indexed_vector_codegen(comp_tree_t* node){
  comp_tree_t* identifier_node = node->first;
  comp_tree_t* curr_expr_node = node->last;
  list_args_info_t* curr_dim = identifier_node->value->symbol_table_entry->args_info;

  node->value->value_register = generateRegister();
  char* temp_register = generateRegister();

  iloc_code_t* curr_last_instruction =
    new_iloc(LOADI, generateConst(0), node->value->value_register, NULL, NULL);

  long long int multi_dim = getTypeSize(identifier_node->value->symbol_table_entry->primitive_type);
  while (curr_expr_node != identifier_node){

      aritmetic_expression_codegen(curr_expr_node);
      iloc_code_t* first_iloc_instr_of_curr_expr_node = getFirstIlocInstruction(curr_expr_node);
      first_iloc_instr_of_curr_expr_node->prev = curr_last_instruction;
      curr_last_instruction = curr_expr_node->value->last_instruction;

      curr_last_instruction = new_iloc(MULTI, curr_expr_node->value->value_register,
	     generateConst(multi_dim)
	     ,temp_register, curr_last_instruction);
      curr_last_instruction = new_iloc(ADD, temp_register,
	  node->value->value_register
	 ,node->value->value_register, curr_last_instruction);

      multi_dim = multi_dim * curr_dim->info;

      curr_expr_node = curr_expr_node->prev;
      curr_dim = curr_dim->next;
  }

  curr_last_instruction = new_iloc(ADDI, node->value->value_register,
	     generateConst(identifier_node->value->symbol_table_entry->offset)
	     ,node->value->value_register, curr_last_instruction);

  char* base_register;
  if (identifier_node->value->symbol_table_entry->scope == GLOBAL_SCP)
    base_register = rbss;
  else
    base_register = rarp;


  curr_last_instruction =
    new_iloc(ADD, base_register, node->value->value_register, node->value->value_register, curr_last_instruction);

  node->value->last_instruction = curr_last_instruction;
}

void variable_codegen(comp_tree_t* node){
  node->value->value_register = generateRegister();

  char* base_register;
  if (node->value->symbol_table_entry->scope == GLOBAL_SCP)
    base_register = rbss;
  else
    base_register = rarp;

  node->value->last_instruction =
    new_iloc(ADDI, base_register, generateConst(node->value->symbol_table_entry->offset), node->value->value_register, NULL);
}

void aritmetic_expression_codegen(comp_tree_t* node) {
    ast_node_value* node_value = (ast_node_value*)(node->value);
    char* temp_register;
    iloc_code_t* curr_last_instruction;

    switch(node_value->semantic_type) {
        case AST_LITERAL:
            node_value->value_register = generateRegister();
            node_value->last_instruction =
                new_iloc(LOADI, generateConst(node_value->val.i), node_value->value_register, NULL, NULL);
            break;
        case AST_IDENTIFICADOR:
            variable_codegen(node);
            temp_register = generateRegister();
            curr_last_instruction = node_value->last_instruction;
            node_value->last_instruction = new_iloc(LOAD, node_value->value_register, temp_register, NULL, curr_last_instruction);
            node_value->value_register = temp_register;
            break;
        case AST_VETOR_INDEXADO:
            indexed_vector_codegen(node);
            temp_register = generateRegister();
            curr_last_instruction = node_value->last_instruction;
            node_value->last_instruction = new_iloc(LOAD, node_value->value_register, temp_register, NULL, curr_last_instruction);
            node_value->value_register = temp_register;
            break;
        //case AST_ARIM_INVERSAO: not sure if needed, didn't find a good way of doing it with iloc
        case AST_ARIM_SOMA:
        case AST_ARIM_SUBTRACAO:
        case AST_ARIM_MULTIPLICACAO:
        case AST_ARIM_DIVISAO:
            aritmetic_operation_codegen(node, node_value->semantic_type);
            break;
        default:
            break;
    }
}

void aritmetic_operation_codegen(comp_tree_t* node, int semantic_type) {
    ast_node_value* node_value = (ast_node_value*)(node->value);

    aritmetic_expression_codegen(node->first);
    aritmetic_expression_codegen(node->last);
    iloc_code_t* first_from_last = getFirstIlocInstruction(node->last);

    ast_node_value* first_value = (ast_node_value*)(node->first->value);
    ast_node_value* last_value = (ast_node_value*)(node->last->value);

    first_from_last->prev = first_value->last_instruction;
    node_value->value_register = generateRegister();

    switch(semantic_type) {
        case AST_ARIM_SOMA:
            node_value->last_instruction =
                new_iloc(ADD, first_value->value_register, last_value->value_register, node_value->value_register, last_value->last_instruction);
            break;
        case AST_ARIM_SUBTRACAO:
            node_value->last_instruction =
                new_iloc(SUB, first_value->value_register, last_value->value_register, node_value->value_register, last_value->last_instruction);
            break;
        case AST_ARIM_MULTIPLICACAO:
            node_value->last_instruction =
                new_iloc(MULT, first_value->value_register, last_value->value_register, node_value->value_register, last_value->last_instruction);
            break;
        case AST_ARIM_DIVISAO:
            node_value->last_instruction =
                new_iloc(DIV, first_value->value_register, last_value->value_register, node_value->value_register, last_value->last_instruction);
            break;
        default:
            break;
	    }
}

iloc_code_t* getFirstIlocInstruction(comp_tree_t* node){
  iloc_code_t* iloc_code = node->value->last_instruction;
  while (iloc_code->prev != NULL) iloc_code = iloc_code->prev;
  return iloc_code;
}

char* generateLabel() {
	static int labelNumber = 0;
	char* label = (char*)malloc(sizeof(char)*12); //max size of a label is 11, 10 for the integer and one for the char
	sprintf(label, "l%i", labelNumber++);
	return label;
}

char* generateRegister() {
	static int registerNumber = 0;
	char* registerName = (char*)malloc(sizeof(char)*12); //max size of a reg is 11, 10 for the integer and one for the char
	sprintf(registerName, "r%i", registerNumber++);
	return registerName;
}

char* generateConst(long long int constValue) {
    char* constString = (char*)malloc(sizeof(char)*50); 
    sprintf(constString, "%lld", constValue);
    return constString;
}

iloc_code_t* new_iloc(int instruction_type,
    char* first_operator,
    char* second_operator,
    char* third_operator,
    iloc_code_t* previous_instruction) {
        iloc_code_t* new_instruction = (iloc_code_t*)malloc(sizeof(iloc_code_t));
        new_instruction->instruction = instruction_type;
        new_instruction->first_operator = first_operator;
        new_instruction->second_operator = second_operator;
        new_instruction->third_operator = third_operator;
        new_instruction->prev = previous_instruction;
        return new_instruction;
    }

ast_node_value* alloc_ast_value(int semantic_type){
    ast_node_value *value = (ast_node_value*)malloc(sizeof(ast_node_value));
    value->semantic_type = semantic_type;
    value->primitive_type = IKS_UNKNOWN_TYPE;
    return value;
}

void init_scope() {
    symbolTable = dict_new();
    scopeStack = push_list(scopeStack, symbolTable);
    allScopes = push_list(allScopes, symbolTable);
}

void end_scope(){
    scopeStack = remove_top_list(scopeStack);
    symbolTable = get_top_item_list(scopeStack);
}

int getTypeSize(int type){
  switch(type){
       case IKS_INT: return SIZE_INT; break;
       case IKS_FLOAT: return SIZE_FLOAT; break;
       case IKS_CHAR: return SIZE_CHAR; break;
       case IKS_BOOL: return SIZE_BOOL; break;
       default: return SIZE_STRING; break;
  }
  return 0;
}

void declare(int scope, struct symbol_table_value_struct *entry, int identifier_type, int primitive_type, list_args_info_t* args_info){
   if (entry->primitive_type != IKS_UNKNOWN_TYPE){
    	yyerror("Identificador já declarado");
    	exit(IKS_ERROR_DECLARED);
   }

   entry->scope = scope;
   entry->identifier_type = identifier_type;
   entry->primitive_type = primitive_type;
   entry->args_info = args_info;

   if (identifier_type != IKS_FUNCTION){
     entry->offset = calculateOffset(scope);
   }
}

long long int calculateOffset(int scope){
   long long int new_offset = 0;
   comp_list_t* curr_scope = scopeStack;
   while (curr_scope != NULL){

     int dict_index;
     for (dict_index = 0; dict_index < curr_scope->item->size; dict_index++){
       if (!curr_scope->item->data[dict_index])
	 continue;

       struct symbol_table_value_struct *curr_entry = (struct symbol_table_value_struct*) curr_scope->item->data[dict_index]->value;
       if (curr_entry->offset == -1){
	 continue;
       }
       long long int curr_entry_size = getTypeSize(curr_entry->primitive_type);
       
       if (curr_entry->identifier_type == IKS_VECTOR){
	 list_args_info_t* arg = curr_entry->args_info;
	 while (arg != NULL){
	   curr_entry_size = curr_entry_size * arg->info;
	   arg = arg->next;
	 }
       }

       if (new_offset < curr_entry_size + curr_entry->offset){
	 new_offset = curr_entry_size + curr_entry->offset;
       }

     }

     curr_scope = curr_scope->next;
     if (scope == LOCAL_SCP){
       if (curr_scope->next == NULL){
	 break; // dont add the offsets of global scope to the calcul
       }
     }
   }
   return new_offset;
}


/* Returns an error if the identifier wasn't declared and returns the
primitive type of the identifier if it was */
int checkDeclared(struct symbol_table_value_struct *entry){

    int size = strlen(entry->lexeme);
    char* key = (char*)malloc(sizeof(char)*(size + 2));

    //adds type to the key in the table
    strcpy(key, entry->lexeme);
    key[size] = entry->type;
    key[size+1] = '\0';

    comp_list_t* scope = scopeStack;

    while (scope){
      struct symbol_table_value_struct *scopeItem;
      scopeItem = dict_get(scope->item,key);
      if (scopeItem != NULL)
	if (scopeItem->primitive_type != IKS_UNKNOWN_TYPE) {
	  free(key);
	  return scopeItem->primitive_type;
	}
      scope = scope->next;
    }

    free(key);
    yyerror("Identificador não declarado");
    exit(IKS_ERROR_UNDECLARED);
    return IKS_UNKNOWN_TYPE;
}

void errorIdentifierType(int identifierType){
    switch (identifierType){
		case IKS_FUNCTION:
		   yyerror("Identificador deve ser utilizado como função");
		   exit(IKS_ERROR_FUNCTION);
		case IKS_VARIABLE:
		   yyerror("Identificador deve ser utilizado como variavel");
		   exit(IKS_ERROR_VARIABLE);
		case IKS_VECTOR:
		   yyerror("Identificador deve ser utilizado como vetor");
		   exit(IKS_ERROR_VECTOR);
    }
}

void checkValidCoercion(int sourceType, int targetType) {
    if(sourceType == targetType) {
        return;
    }
    switch(sourceType) {
        case IKS_STRING:
            yyerror("Impossível fazer coerção com strings.");
            exit(IKS_ERROR_STRING_TO_X);
            break;
        case IKS_CHAR:
            yyerror("Impossível fazer coerção com chars.");
            exit(IKS_ERROR_CHAR_TO_X);
            break;
        case IKS_INT:
            if(targetType != IKS_FLOAT && targetType != IKS_BOOL) {
                exit(IKS_ERROR_WRONG_TYPE);
            }
            break;
        case IKS_BOOL:
            if(targetType != IKS_FLOAT && targetType != IKS_INT) {
                exit(IKS_ERROR_WRONG_TYPE);
            }
            break;
        case IKS_FLOAT:
            if(targetType != IKS_INT && targetType != IKS_BOOL) {
                exit(IKS_ERROR_WRONG_TYPE);
            }
            break;
        default:
            break;
    }
    return;
}

int getInferredType(int firstType, int secondType) {
    checkValidCoercion(secondType, firstType);
    if(firstType == IKS_INT) {
        switch(secondType) {
            case IKS_INT:
            case IKS_BOOL:
                return IKS_INT;
            case IKS_FLOAT:
                return IKS_FLOAT;
            default:
                exit(IKS_ERROR_WRONG_TYPE);
                return IKS_UNKNOWN_TYPE;
        }
    } else if(firstType == IKS_BOOL) {
        switch(secondType) {
            case IKS_INT:
            case IKS_BOOL:
            case IKS_FLOAT:
                return secondType;
            default:
                exit(IKS_ERROR_WRONG_TYPE);
                return IKS_UNKNOWN_TYPE;
        }
    } else if(firstType == IKS_FLOAT) {
        switch(secondType) {
            case IKS_INT:
            case IKS_BOOL:
            case IKS_FLOAT:
                return IKS_FLOAT;
            default:
                exit(IKS_ERROR_WRONG_TYPE);
                return IKS_UNKNOWN_TYPE;
        }
    }
    exit(IKS_ERROR_WRONG_TYPE);
    return IKS_UNKNOWN_TYPE;
}

void checkParams(list_args_info_t* target, comp_tree_t* provided){
    provided = getFirstElementOfList(provided);

    while (provided != NULL && target != NULL) {
        if(((ast_node_value*)(provided->value))->primitive_type != target->info) {
          yyerror("Tipo de parâmetro passado é incompatível");
          exit(IKS_ERROR_WRONG_TYPE_ARGS);
        }

        provided = provided->next;
        target = target->next;
    }

    if (provided != NULL) {
        yyerror("Excesso de argumentos em chamada de função");
        exit(IKS_ERROR_EXCESS_ARGS);
    }
    else if (target != NULL) {
        yyerror("A função requer mais argumentos");
        exit(IKS_ERROR_MISSING_ARGS);
    }
}

void checkIndexes(list_args_info_t* target, comp_tree_t* provided){
    provided = getFirstElementOfList(provided);

    while (provided != NULL && target != NULL) {
        checkValidCoercion(((ast_node_value*)(provided->value))->primitive_type, IKS_INT);

        provided = provided->next;
        target = target->next;
    }

    if (provided != NULL || target != NULL) {
        yyerror("Numero incorreto de indices para o array");
        exit(IKS_ERROR_NUMBER_INDEXES);
    }
}


struct symbol_table_value_struct* checkIdentifierType(struct symbol_table_value_struct *entry, int targetType){
    int size = strlen(entry->lexeme);
    char* key = (char*)malloc(sizeof(char)*(size + 2));

    //adds type to the key in the table
    strcpy(key, entry->lexeme);
    key[size] = entry->type;
    key[size+1] = '\0';

    comp_list_t* scope = scopeStack;

    struct symbol_table_value_struct *scopeItem;
    while (scope){
      scopeItem = dict_get(scope->item,key);
      scope = scope->next;
      if (scopeItem == NULL)
		continue;
      if (scopeItem->identifier_type == 0)
		continue;
      if (scopeItem->identifier_type == targetType) {
		free(key);
		return scopeItem;
	  }
      else
		break;
    }
	free(key);
    errorIdentifierType(scopeItem->identifier_type);
}

void checkPositiveInt(struct symbol_table_value_struct *entry){
   if (entry->value.i <= 0) {
	yyerror("Literal deve ser positivo");
	exit(SINTATICA_ERRO);
   }
}

list_args_info_t* new_list_args_info(int info, list_args_info_t* next){
  list_args_info_t* new = (list_args_info_t*) malloc(sizeof(list_args_info_t));
  new->info = info;
  new->next = next;
  return new;
}


comp_tree_t* getFirstElementOfList(comp_tree_t* list){
   if (list == NULL){
     return NULL;
   }
   while (list->prev != NULL) list = list->prev;
   return list;
}

int comp_get_line_number (void)
{
    return lineNumber;
}

void yyerror (char const *mensagem)
{
    fprintf (stderr, "%i: %s\n", lineNumber, mensagem); //altere para que apareça a linha
}

void main_init (int argc, char **argv)
{
    lineNumber = 1;
}

void print_iloc_code(iloc_code_t* instr){
  if (instr == NULL) return;
  print_iloc_code(instr->prev);
  //printf("%i %s %s => %s\n", instr->instruction, instr->first_operator, instr->second_operator, instr->third_operator);

  switch(instr->instruction) {
    case ADD:
      printf("%s %s, %s => %s\n", "add", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case SUB:
      printf("%s %s, %s => %s\n", "sub", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case MULT:
      printf("%s %s, %s => %s\n", "mult", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case DIV:
      printf("%s %s, %s => %s\n", "div", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case ADDI:
      printf("%s %s, %s => %s\n", "addI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case SUBI:
      printf("%s %s, %s => %s\n", "subI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case RSUBI:
      printf("%s %s, %s => %s\n", "rsubI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case MULTI:
      printf("%s %s, %s => %s\n", "multI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case DIVI:
      printf("%s %s, %s => %s\n", "divI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case RDIVI:
      printf("%s %s, %s => %s\n", "rdivI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case LSHIFT:
      printf("%s %s, %s => %s\n", "lshift", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case LSHIFTI:
      printf("%s %s, %s => %s\n", "lshiftI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case RSHIFT:
      printf("%s %s, %s => %s\n", "rshift", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case RSHIFTI:
      printf("%s %s, %s => %s\n", "rshiftI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case AND:
      printf("%s %s, %s => %s\n", "and", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case ANDI:
      printf("%s %s, %s => %s\n", "andI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case OR:
      printf("%s %s, %s => %s\n", "or", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case ORI:
      printf("%s %s, %s => %s\n", "orI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case XOR:
      printf("%s %s, %s => %s\n", "xor", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case XORI:
      printf("%s %s, %s => %s\n", "xorI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case LOADI:
      printf("%s %s => %s\n", "loadI", instr->first_operator, instr->second_operator);
      break;
    case LOAD:
      printf("%s %s => %s\n", "load", instr->first_operator, instr->second_operator);
      break;
    case LOADAI:
      printf("%s %s, %s => %s\n", "loadAI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case LOADAO:
      printf("%s %s, %s => %s\n", "loadAO", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CLOAD:
      printf("%s %s => %s\n", "cload", instr->first_operator, instr->second_operator);
      break;
    case CLOADAI:
      printf("%s %s, %s => %s\n", "cloadAI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CLOADAO:
      printf("%s %s, %s => %s\n", "cloadAO", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case STORE:
      printf("%s %s => %s\n", "store", instr->first_operator, instr->second_operator);
      break;
    case STOREAI:
      printf("%s %s => %s, %s\n", "storeAI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case STOREAO:
      printf("%s %s => %s, %s\n", "storeAO", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CSTORE:
      printf("%s %s => %s\n", "cstore", instr->first_operator, instr->second_operator);
      break;
    case CSTOREAI:
      printf("%s %s => %s, %s\n", "cstoreAI", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CSTOREAO:
      printf("%s %s => %s, %s\n", "cstoreAO", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case I2I:
      printf("%s %s => %s\n", "i2i", instr->first_operator, instr->second_operator);
      break;
    case C2C:
      printf("%s %s => %s\n", "c2c", instr->first_operator, instr->second_operator);
      break;
    case C2I:
      printf("%s %s => %s\n", "c2i", instr->first_operator, instr->second_operator);
      break;
    case I2C:
      printf("%s %s => %s\n", "i2c", instr->first_operator, instr->second_operator);
      break;
    case JUMPI:
      printf("%s -> %s\n", "jumpI", instr->first_operator);
      break;
    case JUMP:
      printf("%s -> %s\n", "jump", instr->first_operator);
      break;
    case CBR:
      printf("%s %s => %s, %s\n", "cbr", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CMP_LT:
      printf("%s %s, %s => %s\n", "cmp_LT", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CMP_LE:
      printf("%s %s, %s => %s\n", "cmp_LE", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CMP_EQ:
      printf("%s %s, %s => %s\n", "cmp_EQ", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CMP_GE:
      printf("%s %s, %s => %s\n", "cmp_GE", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CMP_GT:
      printf("%s %s, %s => %s\n", "cmp_GT", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case CMP_NE:
      printf("%s %s, %s => %s\n", "cmp_NE", instr->first_operator, instr->second_operator, instr->third_operator);
      break;
    case LABEL:
      printf("%s:\n", instr->first_operator);
      break;
  }

}

void main_finalize (void)
{
  if (comp_tree_last != NULL){
    comp_tree_t* curr_children = ((comp_tree_t*)comp_tree_last)->first;
    while (curr_children != NULL){
      if (curr_children->value->semantic_type == AST_FUNCAO){
	main_codegen(curr_children);
	((comp_tree_t*)comp_tree_last)->value->last_instruction =
	  curr_children->value->last_instruction;
      }
      curr_children = curr_children->next;
    }
  }
  print_iloc_code(((comp_tree_t*)comp_tree_last)->value->last_instruction);
}





/* ETAPA 1
void comp_print_table (void)
{
    int i;
  	for (i = 0; i < symbolTable->size; i++) {
      	if (symbolTable->data[i]) {
      			comp_dict_item_t* item;
      			item = symbolTable->data[i];
      			while (item) {
        				int* v = item->value;
          			cc_dict_etapa_1_print_entrada (item->key, *v);
          			item = item->next;
        		}
      	}
    }
}
*/

//ETAPA 2
void comp_print_table (void)
{
    int i;
  	for (i = 0; i < symbolTable->size; i++) {
      	if (symbolTable->data[i]) {
      			comp_dict_item_t* item;
      			item = symbolTable->data[i];
      			while (item) {
        			symbol_table_entry *v = (symbol_table_entry*)(item->value);
          			cc_dict_etapa_2_print_entrada (item->key, v->lineNumber, v->type);
          			item = item->next;
        		}
      	}
    }
}
